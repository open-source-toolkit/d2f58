# C/C++ 调用 JLINK.DLL 实现 MCU 烧录软件开发示例

## 项目描述

本项目提供了一个通过 C/C++ 调用 JLINK.DLL 实现 MCU 芯片的 Flash 擦除和 HEX 文件烧录的示例代码。该示例代码展示了如何在桌面环境中自动化进行 MCU 烧录操作，适用于 VS（Visual Studio）和 QT 开发环境。

## 环境配置

1. **JLink 驱动版本**：V7.5 X64  
   其他版本未经验证，建议使用 V7.5 X64 版本。

2. **编译器**：MSVC2017 X64  
   本示例代码使用 MSVC2017 X64 编译器进行编译。

3. **构建工具**：CMake 3.25  
   使用 CMake 3.25 进行项目构建。

## 注意事项

- **JLINK DLL 版本**：本示例中使用的 JLINK DLL 为 64 位版本（JLINK_X64.dll），因此编译版本也必须对应为 X64 版本。
- **GCC 编译器**：关于 GCC 编译器 X64 版本是否能够编译，本人未进行验证，建议使用 MSVC2017 X64 编译器。

## 使用说明

1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo-url.git
   ```

2. **配置环境**：
   确保已安装 JLink 驱动 V7.5 X64 版本，并配置好 MSVC2017 X64 编译器和 CMake 3.25。

3. **编译项目**：
   使用 CMake 生成项目文件，并使用 MSVC2017 X64 编译器进行编译。

4. **运行示例**：
   编译完成后，运行生成的可执行文件，按照提示进行 MCU 烧录操作。

## 贡献

欢迎提交 Issue 和 Pull Request，共同完善本项目。

## 许可证

本项目采用 MIT 许可证，详情请参阅 [LICENSE](LICENSE) 文件。